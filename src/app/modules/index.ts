import { HomeModule } from './home/home.module';
import { ShoppingModule } from './shopping/shopping.module';
import { TravelModule } from './travel/travel.module';

export const pageModules = [
  HomeModule,
  ShoppingModule,
  TravelModule
];
