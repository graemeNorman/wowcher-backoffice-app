import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, Event as NavigationEvent } from '@angular/router';

import { foundationStrings } from './shopping.string';

import * as _ from 'lodash';

@Component({
    selector: 'app-wow-shopping-main',
    templateUrl: 'shopping.template.html',
    styleUrls: ['./shopping.style.scss']
})

export class ShoppingComponent implements OnInit, OnDestroy {
    category_links: any;
    intro: any;
    isParentActive: boolean;
    sub: any;
    activeSection: any;

    constructor(private router: Router) {}

    ngOnInit() {
      this.category_links = [
        {
          name: foundationStrings.icon.name,
          linkTo: '/foundations/icons',
          description: foundationStrings.icon.description,
          header: foundationStrings.header
        },
        {
          name: 'Palette',
          linkTo: '/foundations/palette',
          description: foundationStrings.icon.description,
          header: foundationStrings.header
        },
        {
          name: 'Typography',
          linkTo: '/foundations/typography',
          description: foundationStrings.icon.description,
          header: foundationStrings.header
        },
        {
          name: 'Fonts',
          linkTo: '/foundations/fonts',
          description: foundationStrings.icon.description,
          header: foundationStrings.header
        }
      ];

      this.intro = {
        title: 'Foundation',
        icon: 'icon-foundation'
      };

      this.activeSection = {};

      this.sub = this.router.events.subscribe((event: NavigationEvent) => {
        if (event instanceof NavigationEnd) {
          this.isParentActive = event.url === '/shopping';
          if (!this.isParentActive) {
            this.activeSection = _.find(this.category_links, {'linkTo': event.url});
          }
        }
      });
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }
}
