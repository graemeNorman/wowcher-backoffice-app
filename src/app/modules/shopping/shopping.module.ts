import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingComponent } from './shopping.component';
import { ShoppingRoutingModule } from './shopping-routing.module';

@NgModule({
  imports: [
    ShoppingRoutingModule,
    CommonModule
  ],
  declarations: [
    ShoppingComponent
  ]
})

export class ShoppingModule {}
