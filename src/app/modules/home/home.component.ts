import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-wow-home',
    templateUrl: 'home.template.html',
    styleUrls: ['./home.style.scss']
})

export class HomeComponent implements OnInit {
    intro: any;

    ngOnInit() {
      this.intro = {
        company: 'Wowcher',
        heading: 'Welcome to the Wowcher Styleguide ',
        project: 'style-guide',
        description: 'Wowcher\'s visual UX framework, helping maintain a high level of design with a complex structure.'
      };
    }
}
