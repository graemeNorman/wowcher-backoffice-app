import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule
  ],
  declarations: [
    HomeComponent
  ]
})

export class HomeModule {
  // intro = {
  //   company: 'Wowcher',
  //   heading: 'Introducing ',
  //   project: 'style-guide',
  //   description: 'Wowcher\'s visual UX framework, helping maintain a high level of design with a complex structure.'
  // };
}
