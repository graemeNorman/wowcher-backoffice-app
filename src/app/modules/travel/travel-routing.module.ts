import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

/* Main component */
import { TravelComponent } from './travel.component';

/* Child Component(s) */
import { PaletteComponent } from '../../components/wow-palette/wow.palette.component';

const travelRoutes: Routes = [
  { path: 'travel',  component: TravelComponent,
    children: [
      { path: 'european-city-breaks', component: PaletteComponent },
      { path: 'uk-city-breaks', component: PaletteComponent },
      { path: 'beach-holidays', component: PaletteComponent },
      { path: 'uk-seaside', component: PaletteComponent },
      { path: 'long-haul', component: PaletteComponent },
      { path: '**', redirectTo: '/travel' }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(travelRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class TravelRoutingModule { }
