import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-wow-travel-main',
    templateUrl: 'travel.template.html',
    styleUrls: ['./travel.style.scss']
})

export class TravelComponent implements OnInit, OnDestroy {
    subnav_links: any;
    isParentActive: boolean;
    sub: any;
    activeSection: any;

    constructor(private router: Router) {}

    ngOnInit() {
      this.subnav_links = [
        {
          name: 'European City Breaks',
          linkTo: '/travel/european-city-breaks'
        },
        {
          name: 'UK City Breaks',
          linkTo: '/travel/uk-city-breaks'
        },
        {
          name: 'Beach Holidays',
          linkTo: '/travel/beach-holidays'
        },
        {
          name: 'UK Seaside',
          linkTo: '/travel/uk-seaside'
        },
        {
          name: 'Long Haul &amp; Cruises',
          linkTo: '/travel/long-haul'
        }
      ];
      this.isParentActive = true;
      this.activeSection = {};

      this.sub = this.router.events.subscribe(event => {
        if (event instanceof NavigationEnd) {
          this.isParentActive = event.url === '/travel';
          console.log('event ----------------> ', event);
          console.log('event URL ----------------> ', event.url);
          console.log('isParentActive ----------------> ', this.isParentActive);
        }
      });
    }

    ngOnDestroy() {
      this.sub.unsubscribe();
    }
}
