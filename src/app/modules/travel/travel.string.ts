export const foundationStrings = {
  header: 'UI Kit: Foundations',

  icon: {
    name: 'Icons',
    description: ''
  },

  button: {
    name: 'Buttons',
    description: ''
  },

  form: {
    name: 'Forms',
    description: ''
  }
};
