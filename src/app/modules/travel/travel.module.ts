import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TravelComponent } from './travel.component';
import { TravelRoutingModule } from './travel-routing.module';

@NgModule({
  imports: [
    TravelRoutingModule,
    CommonModule
  ],
  declarations: [
    TravelComponent
  ]
})

export class TravelModule {}
