import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-wow-alert',
  templateUrl: './wow-alert.component.html',
  styleUrls: ['./wow-alert.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class WowAlertComponent { }
