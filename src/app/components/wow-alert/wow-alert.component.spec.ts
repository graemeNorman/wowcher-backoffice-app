import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WowAlertComponent } from './wow-alert.component';

describe('WowAlertComponent', () => {
  let component: WowAlertComponent;
  let fixture: ComponentFixture<WowAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WowAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WowAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
