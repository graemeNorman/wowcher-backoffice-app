import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-wow-palette',
  templateUrl: 'wow.palette.template.html',
  styleUrls: ['./wow.palette.style.scss']
})

export class PaletteComponent implements OnInit {
  basicColors: any[];
  greyColors: any[];
  otherColors: any[];
  uspColors: any[];

  ngOnInit() {
    this.basicColors = [
      {name: 'Primary Brand', hex: '#351947'},
      {name: 'Secondary Brand', hex: '#6B2996'},
    ];

    this.greyColors = [
      {name: 'White', hex: '#FFFFFF'},
      {name: 'Black', hex: '#000000'},
      {name: 'Text Grey', hex: '#3B3B3B'},
      {name: 'Dark Grey', hex: '#606060'},
      {name: 'Medium Grey', hex: '#929292'},
      {name: 'Border Grey', hex: '#E4E4E4'},
      {name: 'Base Grey', hex: '#ECECEC'},
      {name: 'Light Grey', hex: '#F2F2F2'}
    ];

    this.otherColors = [
      {name: 'Activity Orange', hex: '#FF8B00'},
      {name: 'Standard Green', hex: '#48B04B'},
      {name: 'Standard Red', hex: '#D62D28'}
    ];

    this.uspColors = [
      {name: 'Primary USP', hex: '#896806'}
    ];
  }
}

@Component({
    selector: 'app-wow-palette-card',
    templateUrl: 'wow.palette-card.template.html',
    styleUrls: ['./wow.palette.style.scss']
})

export class PaletteCardComponent {
  @Input() color: any;
}
