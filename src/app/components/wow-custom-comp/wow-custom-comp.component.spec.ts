import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { WowCustomCompComponent } from './wow-custom-comp.component';

describe('WowCustomCompComponent', () => {
  let component: WowCustomCompComponent;
  let fixture: ComponentFixture<WowCustomCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WowCustomCompComponent ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WowCustomCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain a title', () => {
    fixture = TestBed.createComponent(WowCustomCompComponent);
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h3').textContent).toContain('Wowcher custom component');
    expect(compiled.querySelector('p').textContent).toContain('Specific custom text - test test test!');
  });

  it('should click the Submit button', async(() => {
    spyOn(component, 'exampleSubmit');

    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();

    fixture.whenStable().then(() => {
      expect(component.exampleSubmit).toHaveBeenCalled();
      console.log('How to test click contents!!');
      // const compiled = fixture.debugElement.nativeElement;
      // expect(compiled.querySelector('p').textContent).toContain('You clicked submit!');
    });
  }));

});

