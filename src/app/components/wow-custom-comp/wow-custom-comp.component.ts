import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-wow-custom-comp',
  templateUrl: './wow-custom-comp.component.html',
  styleUrls: ['./wow-custom-comp.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WowCustomCompComponent implements OnInit {

  isSubmitted = false;

  ngOnInit() {}

  exampleSubmit() {
    this.isSubmitted = true;
    // console.log(details.name, 'from ' + details.company + ' you just clicked me!!!');
  }

}
