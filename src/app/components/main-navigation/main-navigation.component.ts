import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wow-main-navigation',
  templateUrl: 'main-navigation.template.html',
  styleUrls: ['./main-navigation.component.scss']
})

export class NavigationComponent implements OnInit {
  navigationItems: any;

  ngOnInit() {
    this.navigationItems = [
      {
        name: 'Shopping',
        linkTo: '/shopping'
      },
      {
        name: 'Travel',
        linkTo: '/travel'
      },
      {
        name: 'Beauty',
        linkTo: '/beauty'
      },
      {
        name: 'Fashion',
        linkTo: '/fashion'
      },
      {
        name: 'Home',
        linkTo: '/home'
      }
    ];

  }
}

