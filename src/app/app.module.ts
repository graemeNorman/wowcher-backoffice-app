import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';

// Routing
import { AppRoutingModule } from './app.routing';
import { pageModules } from './modules/index';

// Global Import(s)
import { NavigationComponent } from './components/main-navigation/main-navigation.component';

// Custom Imports
import { WowAlertComponent } from './components/wow-alert/wow-alert.component';
import { WowCustomCompComponent } from './components/wow-custom-comp/wow-custom-comp.component';
import { PaletteComponent } from './components/wow-palette/wow.palette.component';
import { PaletteCardComponent } from './components/wow-palette/wow.palette.component';

@NgModule({
  declarations: [
    AppComponent,
    WowAlertComponent,
    WowCustomCompComponent,
    PaletteComponent,
    PaletteCardComponent,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    ...pageModules,
    AppRoutingModule,
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
